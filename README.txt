-- Summary --

The purpose of this module is there is no contrib module which lets you
to resize images to an arbitrary size.

This functionality is used in many Wordpress theme via the external
TimThumb image resizer, http://timthumb.googlecode.com. Installing such
module is an alien body in Drupal and cradle of many exploits.

This functionality tries to be a better solution. You can
* incorporate it with other Image module style effect
* Clean URL support
* Caches the rendered image, like as the normal "imagecache"

-- Requirements --

Drupal Core 7.0+

-- Installation --

Enable the module.

$preview_file = _image_resizer_style_path('my_cropper_style', $portfolio->file_uri, '640x400');
$preview_url = file_create_url($preview_file);

The $preview_url should be something like
http://nailartinlondon.co.uk/sites/default/files/ir/a_crop/public/640x400/portfolio/10/tiger_lillies_t.jpg

Change the the 640x480 to any dimension you would like, and wonder. :)

-- Demo --

Frontpage of http://nailartinlondon.co.uk
