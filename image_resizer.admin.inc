<?php

/**
 * @file
 * Administration forms for the Image Resizer module.
 */

/**
 * Menu callback; Displays the administration settings for
 * Image Resizer.
 */
function image_resizer_admin_settings() {
  return system_settings_form(array(
    'prefix' => array(
      '#type' => 'textfield',
      '#title' => t('Prefix'),
      '#default_value' => variable_get('image_resizer_prefix', 'ir'),
    ),
  );
}

